export default {
    "Moses":["JPN", "GER", "ARG"],
    "Mahesh":["BEL", "ENG", "POR"],
    "Tanner":["BRA", "ESP", "URU"],
    "Jim":["BRA", "ENG","CRC"],
    "Josh":["BEL","ENG","SWE"],
    "Gerry":["GER", "BRA", "ARG"],
    "Joel":["BRA", "FRA", "ENG"],
    "Arman":["GER", "BRA", "POR"],
    "Clay":["GER", "BRA", "ARG"],
    "Mike":["GER", "BRA", "FRA"],
    "Aneesh":["GER", "BRA", "ESP"],
    "Erinn":["ARG", "POR", "URU"],
    "Charise":["ENG", "CRO", "AUS"],
    "Asim":["GER", "ARG", "POR"],
    "Tai":["ENG", "POR", "CRO"],
    "Meghan":["BRA", "ARG", "ENG"],
    "Andy":["POR", "CRO", "ENG"],
    "Annie":["BRA", "MEX", "NGA"],
    "Jenna":["GER", "ARG", "AUS"],
    "George":["GER", "BEL", "ENG"]
}
