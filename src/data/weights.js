export default {  
    "RUS": 30,
    "KSA":10,
    "EGY": 150, //EGYPT
    "URU": 25,
    "POR": 20,
    "ESP": 7, // SPA
    "MAR": 250, // MOR?
    "IRN": 250, // IRAN
    "FRA": 6,
    "AUS": 250, 
    "PER": 200, // peru
    "DEN":10,
    "ARG": 8,
    "ISL":10,
    "CRO": 30,
    "NGA": 200, // NIG
    "BRA": 5,
    "SUI":10,
    "CRC": 250, //CR?
    "SRB": 150, //SERB
    "GER": 5,
    "MEX": 60,
    "SWE": 80,
    "KOR":10,
    "BEL": 12,
    "PAN":10,
    "TUN": 500,
    "ENG": 20,
    "POL":10,
    "SEN": 150,
    "COL": 25,
    "JPN": 150 // JAP
 }