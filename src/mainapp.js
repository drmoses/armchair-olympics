import React from 'react';


const MaxPoints = ({ maxPoints }) => (<div>Best Possible Score: 
  <span id="max-point">{maxPoints}</span>  (1 point group win, 0.5 points group tie, 10 points 16 win, 100 points 8 win, 1000 points 4 win, 500 losers bracket win, 10000 champion win)</div>
);
const BestPicks = ( { rankings } ) => (<div>Best Picks: 
  <div id="best-pick">
  {
    rankings.slice(0,3).map((entry) => createEntry(entry))
  }
  </div>
</div>);

const createEntry = (entry, points) => (<span>
  <img src = {entry.flag} alt={entry.name+ '[' + entry.point +']'} title={entry.name + '[' + entry.point +']'} />
  <em className="medalScore">{(entry.medalCount || points[entry.fifaCode]) + 'x' + entry.weight}</em>
</span>);
const getClass = rank => {
  switch(rank) {
    case 1: return 'gold';
    case 2: return 'silver';
    case 3: return 'bronze';
    default: return 'grey';
  }
};
const Standings = ({standings, points}) => (<table>
  <tr><th>Name</th><th>Teams</th><th>Rank</th><th>Score</th></tr>
  {standings.map(({ person, entries, rank, score }) => 
    (<tr className={getClass(rank)} >
      <td>{person}</td>
      <td>{entries.map(entry => createEntry(entry, points))}</td>
      <td>{rank}</td>
      <td>{score}</td>
      </tr>))
  }
</table>);


const App = ({ standings, points }) => {
  return (<div>
    <h1>Armchair Fooballers</h1>
    <em>Last Updated: {points.lastUpdated.toString()}</em>
    <MaxPoints maxPoints={standings.maxPoints} />
    <BestPicks rankings={standings.rankingByWeight} />
    <Standings standings={standings.standings} points={points.points}/>
  </div>);
}
export default App;