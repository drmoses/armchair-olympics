import React from 'react';
import { renderToString } from 'react-dom/server';
import express from 'express';
import App from './mainapp';
import middleware from './aggregate';

const app = express();
app.listen(process.env.PORT || 8080)

app.use(express.static('public_html'));

app.get('/', [
	middleware,
	(req, res) => {
	let html = renderToString(<App {...req.body} />);
	// send it back wrapped up as an HTML5 document:
	
	res.send(`<!DOCTYPE html><html>
	<head>
		<link rel="stylesheet" href="client.css">
	</head>
	<body>${html}<script>
	const __REACT_INITIAL_STATE = ${JSON.stringify(req.body)};
	</script></body></html>`);
}]);
